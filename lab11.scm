(define add(lambda (m n s z)( m s (n s z))))

(define subtract(lambda (m n)( m pred n)))

(define OR(lambda (m n)( m true n)))

(define AND(lambda (m n)( m n false)))

(define LEQ(lambda (m n)( isZero(subtract m n))))

(define EQ(lambda (m n)(AND (LEQ m n)(LEQ n m))))

(define LT(lambda (m n)( not(or( and(LEQ m n)(LEQ n m))(not (LEQ m n))))))

(define forloop(lambda (s f)(EQ s 0) 0 (add f(s-1) s)))
(define Y(lambda (f)(lambda (x)(f(x x)))(lambda (x)(f(x x)))))

(define ifelse(lambda (m n)(OR(isZero(m))(LT(m n)))(Y forloop(n forloop))(add n m)))
